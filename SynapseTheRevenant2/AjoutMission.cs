﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    public partial class AjoutMission : Form
    {
        public AjoutMission()
        {
            InitializeComponent();
            
           
        }

        private void AjoutMission_Load(object sender, EventArgs e)
        {
            Load_Executants();
            Load_Projets();

        }

        private void Load_Executants()
        {
            comboBoxExecutant.DataSource = Intervenant.FetchAll();
            comboBoxExecutant.ValueMember = "Nom";
            comboBoxExecutant.DisplayMember = "Nom";
        }

        private void Load_Projets()
        {
            comboBoxProjet.DataSource = Projet.FetchAll();
            comboBoxProjet.ValueMember = "NomProjet";
            comboBoxProjet.DisplayMember = "nom";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxNom.Text) || string.IsNullOrWhiteSpace(textBoxNbHeuresPrevues.Text) || string.IsNullOrWhiteSpace(richTextBoxDescription.Text))
            {
                MessageBox.Show("T MORT REMPLI LES CHAMPS", "Synapse",
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }
            else
            {
                Mission uneMission = new Mission();
                uneMission.Nom = textBoxNom.Text;
                uneMission.Description = richTextBoxDescription.Text;
                uneMission.NbHeuresPrevues = Convert.ToDecimal(textBoxNbHeuresPrevues.Text);
                uneMission.Intervenant = (Intervenant) comboBoxExecutant.SelectedItem;
                uneMission.Projet = (Projet) comboBoxProjet.SelectedItem;

                uneMission.Save();
                MessageBox.Show("La mission a été ajoutée", "Synapse",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }
        }
    }
}
