﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    public partial class AjoutIntervenant : Form
    {
        public AjoutIntervenant()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxNom.Text) || string.IsNullOrWhiteSpace(textBoxTauxHoraire.Text))
            {
                MessageBox.Show("T MORT REMPLI LES CHAMPS", "Synapse",
                 MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }
            else
            {
                Intervenant unIntervenant = new Intervenant();
                unIntervenant.Nom = textBoxNom.Text;
                unIntervenant.TauxHoraire = Convert.ToDecimal(textBoxTauxHoraire.Text);

                unIntervenant.Save();
                MessageBox.Show("L'intervenant a été ajoutée", "Synapse",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }
        }
    }
}
