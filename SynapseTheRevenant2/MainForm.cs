﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SynapseTheRevenant2
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void ajouterUnProjetToolStripMenuItem_Click(object sender, EventArgs e)
        {
             
         AjoutProjet newMDIChild = new AjoutProjet();  
         // Set the Parent Form of the Child window.  
         newMDIChild.MdiParent = this;  
         // Display the new form.  
         newMDIChild.Show();  
        

        }

        private void ajouterUneMissionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutMission newMDIChild = new AjoutMission();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form  
            newMDIChild.Show();
        }

        private void ajouterUnRelevéToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AjoutReleveHoraire newMDIChild = new AjoutReleveHoraire();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form  
            newMDIChild.Show();
        }

        private void ajouterUnProjetToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AjoutIntervenant newMDIChild = new AjoutIntervenant();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form  
            newMDIChild.Show();
        }

        private void listeDesInToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListeIntervenant newMDIChild = new ListeIntervenant();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form  
            newMDIChild.Show();
        }

        private void listeDesProjetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListeProjet newMDIChild = new ListeProjet();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form  
            newMDIChild.Show();
        }

        private void listeDesMissionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ListeMission newMDIChild = new ListeMission();
            // Set the Parent Form of the Child window.  
            newMDIChild.MdiParent = this;
            // Display the new form  
            newMDIChild.Show();
        }

		private void listesDesRelevésToolStripMenuItem_Click(object sender, EventArgs e)
		{
			ListeReleveHoraire newMDIChild = new ListeReleveHoraire();
			// Set the Parent Form of the Child window.  
			newMDIChild.MdiParent = this;
			// Display the new form  
			newMDIChild.Show();
		}
	}
}
