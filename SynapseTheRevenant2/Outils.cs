﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    class Outils
    {

        public static bool SiDateAvantAjd(DateTime input)
        {          
            return input < DateTime.Now;
        }

        public static void MessagePopUp(string message)
        {
            MessageBox.Show(message, "Synapse",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
        }
    }
}
