﻿namespace SynapseTheRevenant2
{
    partial class AjoutProjet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Nom = new System.Windows.Forms.Label();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxPrix = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.monthCalendarFin = new System.Windows.Forms.MonthCalendar();
            this.monthCalendarDebut = new System.Windows.Forms.MonthCalendar();
            this.ButtonAjoutProjet = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Nom
            // 
            this.Nom.AutoSize = true;
            this.Nom.Location = new System.Drawing.Point(19, 33);
            this.Nom.Name = "Nom";
            this.Nom.Size = new System.Drawing.Size(29, 13);
            this.Nom.TabIndex = 0;
            this.Nom.Text = "Nom";
            // 
            // textBoxNom
            // 
            this.textBoxNom.Location = new System.Drawing.Point(54, 30);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(100, 20);
            this.textBoxNom.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 85);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Début";
            // 
            // textBoxPrix
            // 
            this.textBoxPrix.Location = new System.Drawing.Point(54, 56);
            this.textBoxPrix.Name = "textBoxPrix";
            this.textBoxPrix.Size = new System.Drawing.Size(100, 20);
            this.textBoxPrix.TabIndex = 7;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(19, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(24, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Prix";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 265);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(21, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Fin";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.monthCalendarFin);
            this.groupBox1.Controls.Add(this.monthCalendarDebut);
            this.groupBox1.Controls.Add(this.ButtonAjoutProjet);
            this.groupBox1.Controls.Add(this.textBoxNom);
            this.groupBox1.Controls.Add(this.textBoxPrix);
            this.groupBox1.Controls.Add(this.Nom);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(301, 479);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Ajouter un projet";
            // 
            // monthCalendarFin
            // 
            this.monthCalendarFin.Location = new System.Drawing.Point(54, 265);
            this.monthCalendarFin.MaxSelectionCount = 1;
            this.monthCalendarFin.Name = "monthCalendarFin";
            this.monthCalendarFin.TabIndex = 10;
            // 
            // monthCalendarDebut
            // 
            this.monthCalendarDebut.Location = new System.Drawing.Point(54, 88);
            this.monthCalendarDebut.MaxSelectionCount = 1;
            this.monthCalendarDebut.Name = "monthCalendarDebut";
            this.monthCalendarDebut.TabIndex = 9;
            // 
            // ButtonAjoutProjet
            // 
            this.ButtonAjoutProjet.Location = new System.Drawing.Point(206, 439);
            this.ButtonAjoutProjet.Name = "ButtonAjoutProjet";
            this.ButtonAjoutProjet.Size = new System.Drawing.Size(75, 23);
            this.ButtonAjoutProjet.TabIndex = 9;
            this.ButtonAjoutProjet.Text = "Valider";
            this.ButtonAjoutProjet.UseVisualStyleBackColor = true;
            this.ButtonAjoutProjet.Click += new System.EventHandler(this.ButtonAjoutProjet_Click);
            // 
            // AjoutProjet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 511);
            this.Controls.Add(this.groupBox1);
            this.Name = "AjoutProjet";
            this.Text = "AjoutProjet";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Nom;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxPrix;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MonthCalendar monthCalendarFin;
        private System.Windows.Forms.MonthCalendar monthCalendarDebut;
        private System.Windows.Forms.Button ButtonAjoutProjet;
    }
}