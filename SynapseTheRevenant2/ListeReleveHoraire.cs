﻿using Synapse;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SynapseTheRevenant2
{
	public partial class ListeReleveHoraire : Form
	{
		public ListeReleveHoraire()
		{
			InitializeComponent();
		}
		private void ListeReleveHoraire_Load(object sender, EventArgs e)
		{
			List<Mission> ListeReleveHoraire = Mission.FetchAll();
			//dataGridView1.Columns[0].HeaderText = "NOM";
			//dataGridView1.Columns[2].HeaderText = "TAUX HORAIRE";
			dataGridView1.DataSource = ListeReleveHoraire;
			dataGridView1.Columns["id"].Visible = false;
			dataGridView1.Columns[1].HeaderText = "NOM PROJET";
			dataGridView1.Columns[2].HeaderText = "NOM MISSION";
			dataGridView1.Columns[3].HeaderText = "DATE ";
			dataGridView1.Columns[4].HeaderText = "NOMBRES HEURES";
		}
	}
}
