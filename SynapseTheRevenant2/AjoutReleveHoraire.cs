﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    public partial class AjoutReleveHoraire : Form
    {
        public AjoutReleveHoraire()
        {
            InitializeComponent();
        }

        private void AjoutReleveHoraire_Load(object sender, EventArgs e)
        {
            Load_Projets();
        }

        private void Load_Mission(Projet unProjet)
        {
            comboBoxMissions.DataSource = Mission.FetchAllByProjet(unProjet);
            comboBoxMissions.ValueMember = "Nom";
            comboBoxMissions.DisplayMember = "Nom";
        }

        private void Load_Projets()
        {
            comboBoxProjet.DataSource = Projet.FetchAll();
            comboBoxProjet.ValueMember = "NomProjet";
            comboBoxProjet.DisplayMember = "nom";
        }

        private void comboBoxProjet_SelectedValueChanged(object sender, EventArgs e)
        {
            Projet unProjet = (Projet)comboBoxProjet.SelectedItem;
            Load_Mission(unProjet);
        }

        /// <summary>
        ///  Click on validate button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxNbHeuresPrevues.Text))
            {
                Outils.MessagePopUp("T MORT REMPLI LES CHAMPS");
            }
            else
            {
                Mission uneMission = (Mission)comboBoxMissions.SelectedItem;
                uneMission.ReleveHoraire.Add(dateTimePicker1.Value.TimeOfDay, Convert.ToInt32(textBoxNbHeuresPrevues.Text));
                uneMission.Save();
                Outils.MessagePopUp("Le relevé a été ajouté");
            
            }
        }
    }
}