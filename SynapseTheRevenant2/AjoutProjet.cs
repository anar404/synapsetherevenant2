﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    public partial class AjoutProjet : Form
    {
        public AjoutProjet()
        {
            InitializeComponent();
        }



        private void ButtonAjoutProjet_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(textBoxNom.Text) || string.IsNullOrWhiteSpace(textBoxPrix.Text))
            {
                Outils.MessagePopUp("Les champs de textes sont vides");

            }
            else if (!Information.IsNumeric(textBoxPrix.Text))
            {
                Outils.MessagePopUp("Le Prix saisi n'est pas en valeur numérique");
            }

            else if (Outils.SiDateAvantAjd(monthCalendarDebut.SelectionStart))
            {
                Outils.MessagePopUp("La date de début du projet selectionnée ne peut être antérieure à la date d'aujourd'hui ni aujoud'hui"); 
            }
            else if (monthCalendarFin.SelectionStart < monthCalendarDebut.SelectionStart)
            {
                Outils.MessagePopUp("La date de fin du projet est avant celle de début.");
            }
            
            else
            {
                Projet unProjet = new Projet();
                unProjet.NomProjet = textBoxNom.Text;
                unProjet.DateDebutProjet = monthCalendarDebut.SelectionRange.Start;
                unProjet.DateFinProjet = monthCalendarFin.SelectionRange.Start;
                unProjet.PrixFactureMO = Convert.ToDecimal(textBoxPrix.Text);

                unProjet.Save();
                MessageBox.Show("Le projet a été ajouté", "Synapse",
                    MessageBoxButtons.OKCancel, MessageBoxIcon.Asterisk);
            }


        }
    }
}
