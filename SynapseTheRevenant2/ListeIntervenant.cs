﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    public partial class ListeIntervenant : Form
    {
        public ListeIntervenant()
        {
            InitializeComponent();
        }

        private void ListeIntervenant_Load(object sender, EventArgs e)
        {
            LoadDataGridView();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadDataGridView();
        }

        private void LoadDataGridView()
        {
            List<Intervenant> ListeIntervenant = Intervenant.FetchAll();
            //dataGridView1.Columns[0].HeaderText = "NOM";
            //dataGridView1.Columns[2].HeaderText = "TAUX HORAIRE";
            dataGridView1.DataSource = ListeIntervenant;
            dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns[1].HeaderText = "NOM";
            dataGridView1.Columns[2].HeaderText = "TAUX HORAIRE";
        }
    }
}
