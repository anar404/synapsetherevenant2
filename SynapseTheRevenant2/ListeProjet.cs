﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse;

namespace SynapseTheRevenant2
{
    public partial class ListeProjet : Form
    {
        public ListeProjet()
        {
            InitializeComponent();
        }

        private void ListeProjet_Load(object sender, EventArgs e)
        {
            List<Projet> ListeIntervenant = Projet.FetchAll();
            //dataGridView1.Columns[0].HeaderText = "NOM";
            //dataGridView1.Columns[2].HeaderText = "TAUX HORAIRE";
            dataGridView1.DataSource = ListeIntervenant;
            dataGridView1.Columns["id"].Visible = false;
            dataGridView1.Columns[1].HeaderText = "NOM PROJET";
            dataGridView1.Columns[2].HeaderText = "DATE DEBUT";
            dataGridView1.Columns[3].HeaderText = "DATE FIN";
            dataGridView1.Columns[4].HeaderText = "PRIX FACTURE ( en €)";
        }
    }
}
