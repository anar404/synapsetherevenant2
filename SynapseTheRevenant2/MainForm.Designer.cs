﻿namespace SynapseTheRevenant2
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.intervenantsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ajouterUnProjetToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
			this.listeDesInToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.projetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ajouterUnProjetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.listeDesProjetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.missionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ajouterUneMissionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.listeDesMissionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.relevéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.ajouterUnRelevéToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.listesDesRelevésToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.intervenantsToolStripMenuItem,
            this.projetToolStripMenuItem,
            this.missionsToolStripMenuItem,
            this.relevéToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(962, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// intervenantsToolStripMenuItem
			// 
			this.intervenantsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterUnProjetToolStripMenuItem1,
            this.listeDesInToolStripMenuItem});
			this.intervenantsToolStripMenuItem.Name = "intervenantsToolStripMenuItem";
			this.intervenantsToolStripMenuItem.Size = new System.Drawing.Size(84, 20);
			this.intervenantsToolStripMenuItem.Text = "Intervenants";
			// 
			// ajouterUnProjetToolStripMenuItem1
			// 
			this.ajouterUnProjetToolStripMenuItem1.Name = "ajouterUnProjetToolStripMenuItem1";
			this.ajouterUnProjetToolStripMenuItem1.Size = new System.Drawing.Size(193, 22);
			this.ajouterUnProjetToolStripMenuItem1.Text = "Ajouter un Intervenant";
			this.ajouterUnProjetToolStripMenuItem1.Click += new System.EventHandler(this.ajouterUnProjetToolStripMenuItem1_Click);
			// 
			// listeDesInToolStripMenuItem
			// 
			this.listeDesInToolStripMenuItem.Name = "listeDesInToolStripMenuItem";
			this.listeDesInToolStripMenuItem.Size = new System.Drawing.Size(193, 22);
			this.listeDesInToolStripMenuItem.Text = "Liste des Intervenants";
			this.listeDesInToolStripMenuItem.Click += new System.EventHandler(this.listeDesInToolStripMenuItem_Click);
			// 
			// projetToolStripMenuItem
			// 
			this.projetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterUnProjetToolStripMenuItem,
            this.listeDesProjetsToolStripMenuItem});
			this.projetToolStripMenuItem.Name = "projetToolStripMenuItem";
			this.projetToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
			this.projetToolStripMenuItem.Text = "Projet";
			// 
			// ajouterUnProjetToolStripMenuItem
			// 
			this.ajouterUnProjetToolStripMenuItem.Name = "ajouterUnProjetToolStripMenuItem";
			this.ajouterUnProjetToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.ajouterUnProjetToolStripMenuItem.Text = "Ajouter un projet";
			this.ajouterUnProjetToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnProjetToolStripMenuItem_Click);
			// 
			// listeDesProjetsToolStripMenuItem
			// 
			this.listeDesProjetsToolStripMenuItem.Name = "listeDesProjetsToolStripMenuItem";
			this.listeDesProjetsToolStripMenuItem.Size = new System.Drawing.Size(164, 22);
			this.listeDesProjetsToolStripMenuItem.Text = "Liste des projets";
			this.listeDesProjetsToolStripMenuItem.Click += new System.EventHandler(this.listeDesProjetsToolStripMenuItem_Click);
			// 
			// missionsToolStripMenuItem
			// 
			this.missionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterUneMissionToolStripMenuItem,
            this.listeDesMissionsToolStripMenuItem});
			this.missionsToolStripMenuItem.Name = "missionsToolStripMenuItem";
			this.missionsToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
			this.missionsToolStripMenuItem.Text = "Missions";
			// 
			// ajouterUneMissionToolStripMenuItem
			// 
			this.ajouterUneMissionToolStripMenuItem.Name = "ajouterUneMissionToolStripMenuItem";
			this.ajouterUneMissionToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.ajouterUneMissionToolStripMenuItem.Text = "Ajouter une mission";
			this.ajouterUneMissionToolStripMenuItem.Click += new System.EventHandler(this.ajouterUneMissionToolStripMenuItem_Click);
			// 
			// listeDesMissionsToolStripMenuItem
			// 
			this.listeDesMissionsToolStripMenuItem.Name = "listeDesMissionsToolStripMenuItem";
			this.listeDesMissionsToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
			this.listeDesMissionsToolStripMenuItem.Text = "Liste des missions";
			this.listeDesMissionsToolStripMenuItem.Click += new System.EventHandler(this.listeDesMissionsToolStripMenuItem_Click);
			// 
			// relevéToolStripMenuItem
			// 
			this.relevéToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ajouterUnRelevéToolStripMenuItem,
            this.listesDesRelevésToolStripMenuItem});
			this.relevéToolStripMenuItem.Name = "relevéToolStripMenuItem";
			this.relevéToolStripMenuItem.Size = new System.Drawing.Size(95, 20);
			this.relevéToolStripMenuItem.Text = "Relevé Horaire";
			// 
			// ajouterUnRelevéToolStripMenuItem
			// 
			this.ajouterUnRelevéToolStripMenuItem.Name = "ajouterUnRelevéToolStripMenuItem";
			this.ajouterUnRelevéToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
			this.ajouterUnRelevéToolStripMenuItem.Text = "Ajouter un Relevé horaire";
			this.ajouterUnRelevéToolStripMenuItem.Click += new System.EventHandler(this.ajouterUnRelevéToolStripMenuItem_Click);
			// 
			// listesDesRelevésToolStripMenuItem
			// 
			this.listesDesRelevésToolStripMenuItem.Name = "listesDesRelevésToolStripMenuItem";
			this.listesDesRelevésToolStripMenuItem.Size = new System.Drawing.Size(207, 22);
			this.listesDesRelevésToolStripMenuItem.Text = "Listes des relevés";
			this.listesDesRelevésToolStripMenuItem.Click += new System.EventHandler(this.listesDesRelevésToolStripMenuItem_Click);
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoSize = true;
			this.ClientSize = new System.Drawing.Size(962, 553);
			this.Controls.Add(this.menuStrip1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.IsMdiContainer = true;
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "MainForm";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem projetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnProjetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem missionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUneMissionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem relevéToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnRelevéToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem intervenantsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ajouterUnProjetToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem listeDesInToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesProjetsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem listeDesMissionsToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem listesDesRelevésToolStripMenuItem;
	}
}

