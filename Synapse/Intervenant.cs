﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;


namespace Synapse
{
    /// <summary>
    /// Représente un employé de réalise des missions
    /// </summary>
    public class Intervenant
    {
        #region Modele Objet
        /// <summary>
        /// identifiant de l'intervenant
        /// </summary>
        public short Id { get; private set; } // Primary Key (Bd)
        /// <summary>
        /// Nom de l'intervenant
        /// </summary>
        public string Nom { get; set; }
        /// <summary>
        /// Le revenu horaire de l'intervenant
        /// </summary>
        public decimal TauxHoraire { get; set; }
        /// <summary>
        /// Initialise un intervenant
        /// </summary>
        public Intervenant()
        {
            Id = -1;
            Nom = "";
            /*
             * Identifie un intervenant non référencé dans la base de données
             * */
        }

        #endregion

        #region Méthodes d'accès aux données

        /// <summary>
        /// Retourne un intervenant
        /// </summary>
        /// <param name="idContact">l'identifiant de l'intervenant</param>
        public static Intervenant Fetch(int idIntervenant)
        {
            Intervenant unIntervenant = null;
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();//Initialisation d'un objet permettant d'interroger la bd
            commandSql.CommandText = _selectByIdSql;//Définit la requete à utiliser
            commandSql.Parameters.Add(new MySqlParameter("?id", idIntervenant));//Transmet un paramètre à utiliser lors de l'envoie de la requête
            commandSql.Prepare();//Prépare la requête (modification du paramètre de la requête)
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();//Exécution de la requête
            bool existEnregistrement = jeuEnregistrements.Read();//Lecture du premier enregistrement
            if (existEnregistrement)//Si l'enregistrement existe
            {//alors
                unIntervenant = new Intervenant();//Initialisation de la variable Contact
                unIntervenant.Id = Convert.ToInt16(jeuEnregistrements["id"].ToString());//Lecture d'un champ de l'enregistrement
                unIntervenant.Nom = jeuEnregistrements["Nom"].ToString();
                string tauxHoraire = jeuEnregistrements["tauxHoraire"].ToString();
                unIntervenant.TauxHoraire = Convert.ToDecimal(tauxHoraire);
            }
            openConnection.Close();//fermeture de la connexion 
            return unIntervenant;
        }

        /// <summary>
        /// Sauvegarde l'intervenant courant
        /// </summary>
        public void Save()
        {
            if (Id == -1)
            {
                Insert();
            }
            else
            {
                Update();
            }
        }

        /// <summary>
        /// Supprime l'intervenant courant
        /// </summary>
        public void Delete()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
            Id = -1;
        }

        /// <summary>
        /// Retourne une collection d'intervenant
        /// </summary>
        /// <returns>Une collection de contacts</returns>
        public static List<Intervenant> FetchAll()
        {
            List<Intervenant> resultat = new List<Intervenant>();
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Intervenant unIntervenant = new Intervenant();
                string idIntervenant = jeuEnregistrements["id"].ToString();
                unIntervenant.Id = Convert.ToInt16(idIntervenant);
                unIntervenant.Nom = jeuEnregistrements["nom"].ToString();

                string tauxHoraire = jeuEnregistrements["tauxHoraire"].ToString();
                unIntervenant.TauxHoraire = Convert.ToDecimal(tauxHoraire);
                resultat.Add(unIntervenant);
            }
            openConnection.Close();
            return resultat;
        }

        private void Update()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(new MySqlParameter("?id", Id));
            commandSql.Parameters.Add(new MySqlParameter("?tauxHoraire", TauxHoraire));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            openConnection.Close();
        }

        private void Insert()
        {
            MySqlConnection openConnection = DataBaseAccess.getOpenMySqlConnection();
            MySqlCommand commandSql = openConnection.CreateCommand();
            commandSql.CommandText = _insertSql;

            commandSql.Parameters.Add(new MySqlParameter("?tauxHoraire", TauxHoraire));
            commandSql.Parameters.Add(new MySqlParameter("?nom", Nom));

            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            Id = Convert.ToInt16(commandSql.LastInsertedId);

            openConnection.Close();
        }
        #endregion

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données

        private static string _selectSql =
            "SELECT id ,  nom ,  tauxhoraire  FROM intervenant";

        private static string _selectByIdSql =
            "SELECT id ,  nom ,  tauxhoraire  FROM intervenant WHERE id = ?id ";

        private static string _updateSql =
            "UPDATE intervenant SET nom=?nom, tauxHoraire=?tauxHoraire  WHERE id=?id ";

        private static string _insertSql =
            "INSERT INTO intervenant (nom,tauxHoraire) VALUES (?nom,?tauxHoraire)";

        private static string _deleteByIdSql =
            "DELETE FROM intervenant WHERE id = ?id";

        private static string _getLastInsertId =
            "SELECT id FROM intervenant WHERE nom=?nom AND tauxHoraire=?tauxHoraire   ";
        #endregion
    }
}
