﻿using MySql.Data.MySqlClient;

namespace Synapse
{
    class DataBaseAccess
    {
        public static MySqlConnection getOpenMySqlConnection()
        {           
            MySqlConnection msc = new MySqlConnection("Database=grp1;Data Source=172.16.50.52;User Id=grp1;Password=grp1;Ssl Mode=None;charset=utf8");
            try
            {
                msc.Open();
            }
            catch (MySqlException  mysqlException)
            {
                msc = new MySqlConnection("Database=synapse2;Data Source=localhost;User Id=root;Password=;Ssl Mode=None;charset=utf8");
                msc.Open();
            }
            
            return msc;

        }
    }
}

//Avec Abstraction -> Diminue le COUPLAGE 

//public static IDbConnection Connexion { get; set; }

//public static IDbDataParameter CodeParam(string paramName, object value)
//{
//    IDbCommand commandSql = Connexion.CreateCommand();
//    IDbDataParameter parametre = commandSql.CreateParameter();
//    parametre.ParameterName = paramName;
//    parametre.Value = value;
//    return parametre;
//}